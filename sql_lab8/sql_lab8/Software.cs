namespace sql_lab8
{
    using System;
    using System.Collections.Generic;
    
    public partial class Software
    {
        public int software_ID { get; set; }
        public string name { get; set; }
        public string provider { get; set; }
        public Nullable<double> price { get; set; }
        public Nullable<int> special_offer_ID { get; set; }
    
        public virtual Special_offer Special_offer { get; set; }
    }
}
