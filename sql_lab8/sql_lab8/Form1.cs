﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sql_lab8
{
    public partial class Form1 : Form
    {
        LavrukEntities DB = new LavrukEntities();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cmbSpecial_offer_ID.Items.Clear();
            
            foreach(Special_offer item in DB.Special_offer.ToList())
            {
                cmbSpecial_offer_ID.Items.Add(item);
            }
            cmbSpecial_offer_ID.DisplayMember = "special_offer_ID";
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DB.Add_Software(NameTextBox.Text, ProviderTextBox.Text, PriceTextBox.Text, cmbSpecial_offer_ID.Text);

                MessageBox.Show("Successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}");
            }
        }

        private void buttonShowTable_Click(object sender, EventArgs e)
        {
            dgvAppRes.DataSource = DB.Software.ToList();
        }
    }
}
